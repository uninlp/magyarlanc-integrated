package uni.miskolc.nlp.context;

import hu.u_szeged.dep.parser.MyMateParser;
import hu.u_szeged.magyarlanc.resource.ResourceHolder;
import hu.u_szeged.pos.purepos.MyPurePos;

public class ContextHandler {

	private static ContextHandler instance;

	private MyMateParser myMateParser;

	private MyPurePos myPurePos;

	static {
		instance = new ContextHandler();
	}

	private ContextHandler() {
	}

	public static ContextHandler getInstance() {
		return instance;
	}

	private void initResourceHolder() {
		ResourceHolder.initTokenizer();
		ResourceHolder.initCorpus();
		ResourceHolder.initMSDReducer();
		ResourceHolder.initPunctations();
		ResourceHolder.initRFSA();
		ResourceHolder.initKRToMSD();
		ResourceHolder.initMSDToCoNLLFeatures();
		ResourceHolder.initCorrDic();
		ResourceHolder.initMorPhonDir();
	}

	private void initMorph() {
		initResourceHolder();
		MyPurePos.getInstance();
	}

	private void initDependency() {
		initMorph();
		MyMateParser.getInstance();
	}

	public MyMateParser getMyMateParser() {
		if (myMateParser == null) {
			initDependency();
			myMateParser = MyMateParser.getInstance();
		}

		return myMateParser;
	}

	public MyPurePos getMyPurePos() {
		if (myPurePos == null) {
			initDependency();
			myPurePos = MyPurePos.getInstance();
		}

		return myPurePos;
	}
}
