package uni.miskolc.nlp.model;

public class AnalyzedWord {

	private int id;

	private String form;

	private String lemma;

	private Pos pos;

	private String feature;

	private String pheads;

	private int targetId;
	
	public AnalyzedWord() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getLemma() {
		return lemma;
	}

	public void setLemma(String lemma) {
		this.lemma = lemma;
	}

	public Pos getPos() {
		return pos;
	}

	public void setPos(Pos pos) {
		this.pos = pos;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getPheads() {
		return pheads;
	}

	public void setPheads(String pheads) {
		this.pheads = pheads;
	}

	public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}
}
