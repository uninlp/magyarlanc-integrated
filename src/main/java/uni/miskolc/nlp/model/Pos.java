package uni.miskolc.nlp.model;

public enum Pos {
	SYM,
	INTJ,
	PUNCT,
	CONJ,
	SCONJ,
	DET,
	ADP,
	ADV,
	PRON,
	NOUN,
	PROPN,
	ADJ,
	AUX,
	VERB,
	NUM
}
