package uni.miskolc.nlp.model;

import java.util.List;

public class AnalyzedSentence {

	private List<AnalyzedWord> analyzedWords;

	public AnalyzedSentence() {
	}

	public List<AnalyzedWord> getAnalyzedWords() {
		return analyzedWords;
	}

	public void setAnalyzedWords(List<AnalyzedWord> analyzedWords) {
		this.analyzedWords = analyzedWords;
	}
}
