package uni.miskolc.nlp.parser;

import java.util.List;

import uni.miskolc.nlp.model.AnalyzedSentence;

public interface TextParser {

	List<AnalyzedSentence> parse(String text);
}
