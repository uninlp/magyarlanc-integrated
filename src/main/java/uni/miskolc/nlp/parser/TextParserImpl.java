package uni.miskolc.nlp.parser;

import java.util.ArrayList;
import java.util.List;

import hu.u_szeged.dep.parser.MyMateParser;
import hu.u_szeged.pos.purepos.MyPurePos;
import splitter.MySplitter;
import uni.miskolc.nlp.model.AnalyzedSentence;
import uni.miskolc.nlp.model.AnalyzedWord;
import uni.miskolc.nlp.model.Pos;

public class TextParserImpl implements TextParser {

	private MyPurePos myPurePos;

	private MyMateParser parser;

	private MySplitter mySplitter;
	
	public TextParserImpl(MyPurePos myPurePos, MyMateParser parser, MySplitter mySplitter) {
		this.myPurePos = myPurePos;
		this.parser = parser;
		this.mySplitter = mySplitter;
	}

	public List<AnalyzedSentence> parse(String text) {
		String[][] sentences = mySplitter.splitToArray(text);
		List<AnalyzedSentence> analyzedSentences = new ArrayList<AnalyzedSentence>();

		for (String[] sentence : sentences) {
			String[][] parsedSentence = myPurePos.morphParseSentence(sentence);
			String[][] dep = parser.parseSentence(parsedSentence);
			
			List<AnalyzedWord> analyzedWords = new ArrayList<AnalyzedWord>();

			for (int j = 0; j < dep.length; j++) {
				int id = Integer.valueOf(dep[j][0]);
				String form = dep[j][1];
				String lemma = dep[j][2];
				Pos pos = Pos.valueOf(dep[j][3]);
				String feature = dep[j][4];
				String pheads = dep[j][6];
				int targetId = Integer.valueOf(dep[j][5]);

				AnalyzedWord analyzedWord = new AnalyzedWord();
				analyzedWord.setFeature(feature);
				analyzedWord.setForm(form);
				analyzedWord.setId(id);
				analyzedWord.setLemma(lemma);
				analyzedWord.setPheads(pheads);
				analyzedWord.setPos(pos);
				analyzedWord.setTargetId(targetId);
				
				analyzedWords.add(analyzedWord);
			}

			AnalyzedSentence analyzedSentence = new AnalyzedSentence();
			analyzedSentence.setAnalyzedWords(analyzedWords);

			analyzedSentences.add(analyzedSentence);
		}

		return analyzedSentences;
	}
}
