package uni.miskolc.nlp.test;

import java.util.List;

import org.junit.Test;

import hu.u_szeged.dep.parser.MyMateParser;
import hu.u_szeged.pos.purepos.MyPurePos;
import splitter.MySplitter;
import uni.miskolc.nlp.context.ContextHandler;
import uni.miskolc.nlp.model.AnalyzedSentence;
import uni.miskolc.nlp.model.AnalyzedWord;
import uni.miskolc.nlp.parser.TextParser;
import uni.miskolc.nlp.parser.TextParserImpl;

public class IntegrationTest {

	@Test
	public void integrationTest() {
		MyPurePos myPurePos = ContextHandler.getInstance().getMyPurePos();
		MyMateParser parser = ContextHandler.getInstance().getMyMateParser();
		MySplitter mySplitter = MySplitter.getInstance();
		TextParser textParser = new TextParserImpl(myPurePos, parser, mySplitter);

		List<AnalyzedSentence> parsed = textParser.parse("Ez egy hosszabb szöveg, amit ki lehet próbálni!");

		System.out.println("Ez egy hosszabb szöveg, amit ki lehet próbálni!");

		for (AnalyzedSentence analyzedSentence : parsed) {
			for (AnalyzedWord analyzedWord : analyzedSentence.getAnalyzedWords()) {
				System.out.print(analyzedWord.getId() + "\t");
				System.out.print(analyzedWord.getTargetId() + "\t");
				System.out.print(analyzedWord.getForm() + getExtTab(analyzedWord.getForm()));
				System.out.print(analyzedWord.getPos() + "\t\t");
				System.out.print(analyzedWord.getLemma() + getExtTab(analyzedWord.getLemma()));
				System.out.print(analyzedWord.getPheads() + "\t\t");
				System.out.println(analyzedWord.getFeature() + "\t\t");
			}
		}
	}

	private String getExtTab(String string) {
		return string.length() > 7 ? "\t" : "\t\t";
	}
}
